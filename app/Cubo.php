<?php
namespace Cubo;
ini_set('max_execution_time', 600);

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cubo. Esta clase representa la estructura de una matriz 3D
 * @package Cubo
 * @author Yurani Alejandra Salamanca Lotero
 */
class Cubo
{
    /**
     * Atributo donde se almacenará los valores de cada Query. Representa la matriz 3D
     * @var array. Array multidimensional
     */
    public $matriz;

    /**
     * Esta funcion se encarga de inicializar el cubo
     * Cubo constructor.
     */
    function __construct()
    {
        $this->matriz = [];
    }

    /**
     * Esta funcion se encarga de inicializar la matriz con un valor de cero en cada posicion.
     * La matriz estará compuesta por un array multidimensional donde cada array representa la
     * posicion "x", "y" y "z" que conforma una coordenada
     * @param $tamanio. Tamaño de cada uno de los arrays
     */
    public function inicializarMatriz($tamanio)
    {
         $this->matriz = array_fill(0, $tamanio, array_fill(0, $tamanio, array_fill(0, $tamanio, 0)));
    }

    /**
     * Esta funcion se encarga de sumar los valores que hay en la matriz entre un rango de coordenadas,
     * es decir, entre (x1, y1, z1) y (x2, y2, z2)
     * @param $x1. Representa el valor en "x" de la coordenada inicial
     * @param $y1. Representa el valor de "y" de la coordenada inicial
     * @param $z1. Representa el valor de "z" de la coordenada inicial
     * @param $x2. Representa el valor de "x" de la coordenada final
     * @param $y2. Representa el valor de "y" de la coordenada final
     * @param $z2. Representa el valor de "y" de la coordenada final
     * @return int. Devuelve la suma de los valores
     */
    public function query($x1, $y1, $z1, $x2, $y2, $z2)
    {
        $suma = 0;
        for ($i = $x1; $i <= $x2; $i++){
            for($j = $y1; $j <= $y2; $j++){
                for($k = $z1; $k <= $z2; $k++){
                    $suma += $this->matriz[$i][$j][$k];
                }
            }
        }
        return $suma;
    }

    /**
     * Esta funcion se encarga de actualizar un valor de la matriz en la
     * coordenada especificada
     * @param $x. Posición "x" de la coordenada
     * @param $y. Posición "y" de la coordenada
     * @param $z. Posición "z" de la coordenada
     * @param $valor. Valor a actualizar
     */
    public function update($x, $y, $z, $valor)
    {
        $this->matriz[$x][$y][$z] = $valor;
    }

}
