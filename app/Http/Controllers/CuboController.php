<?php

namespace Cubo\Http\Controllers;

use Cubo\Cubo;
use Cubo\Restriccion;
use Illuminate\Http\Request;

use Cubo\Http\Requests;

/**
 * Class CuboController, permite capturar los datos que provienen de la vista, enviarlos al modelo Cubo
 * para su respectiva logica y posteriormente dar una respuesta a la vista correspondiente.
 * @package Cubo\Http\Controllers
 * @author Yurani Alejandra Salamanca Lotero
 */
class CuboController extends Controller
{
    /**
     * Esta es la función principal para la generar la matriz 3D, dicha funcion permite capturar los datos
     * ingresados por el usuario, realizar el procesamiento de los datos y enviar la respuesta correspondiente
     * @param Request $request
     * @return string
     */
    public function generar(Request $request)
    {
        $cubo           = new Cubo();
        $validacion     = new Restriccion();
        //Se captura el INPUT y se divide por saltos de linea para obtener un Array de Strings
        // por cada linea de texto ingresada
        $valores        = explode("\n", $request->input('valores'));
        $casosPrueba    = intval(trim(current($valores)));
        $respuesta      = [];
        $total          = '';
        if ($validacion->validar('t', $casosPrueba)){
            //Se realiza una iteracion de acuerdo a los casos de prueba ingresados
            while ($casosPrueba >= 1){
                //Se divide la linea que contiene los valores de N y M creando un Array de Strings
                $info = explode(" ",next($valores));
                $tamanioMatriz   = intval($info[0]);
                $cantOperaciones = intval($info[1]);

                if(($validacion->validar('n', $tamanioMatriz)) == false || ($validacion->validar('m', $cantOperaciones)) == false){
                    $respuesta['valido'] = '';
                    $respuesta['invalido'] = 'Las entradas son incorrectas';
                    return $respuesta;
                }
                $cubo->inicializarMatriz($tamanioMatriz);

                //Se realiza una iteración de acuerdo a la cantidad de operaciones ingresadas
                for ($i = 0; $i < $cantOperaciones; $i++){
                    //Se divide la linea que contiene la operacion a ejecutar
                    $operacion = explode(" ", next($valores));
                    $operador  = $operacion[0];
                    //Se realiza la comparación de la operacion
                    if ($operador == "UPDATE"){
                        $cubo->update(intval($operacion[1])-1, intval($operacion[2])-1, intval($operacion[3])-1,
                            intval($operacion[4]));
                    } else {
                        $suma = $cubo->query(intval($operacion[1])-1, intval($operacion[2])-1, intval($operacion[3])-1,
                            intval($operacion[4])-1, intval($operacion[5])-1, intval($operacion[6])-1);
                        //Se concatena cada uno de los valores y se le agrega un salto de linea
                        $total .= $suma."\n";
                    }
                }
                $casosPrueba--;
            }
        }else{
            $respuesta['invalido'] = 'Las entradas son incorrectas';
        }
        $respuesta['valido'] = $total;
        return ($respuesta);
    }
}
