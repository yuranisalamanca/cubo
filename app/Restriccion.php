<?php

namespace Cubo;
/**
 * Class Restriccion. Clase encarga de validar las restricciones que el enunciado de Cube-Summation plantea
 * @package Cubo
 * @author Yurani Alejandra Salamanca Lotero
 */
class Restriccion
{
    /**
     * Esta funcion se encarga de verificar si las entradas cumplen con los criterios establecidos
     * @param $restriccion. Tipo de entrada
     * @param $valor. El valor de la entrada
     * @param int $n. El valor de n (Es necesario para la restricciones de la posicion de una coordenada)
     * @return bool. Retorna true si pasa la validacion y false si no pasa
     */
    public function validar($restriccion, $valor, $n = 0){
        if ($restriccion == 't'){
            if($valor >= 1 && $valor <= 50){
                return true;
            }
        } elseif ($restriccion == 'n'){
            if($valor >= 1 && $valor <= 100){
                return true;
            }
        } elseif ($restriccion == 'm'){
            if($valor >= 1 && $valor <= 1000){
                return true;
            }
        } elseif ($restriccion == 'pos'){
            if($valor >= 1 && $valor <= $n){
                return true;
            }
        }
        return false;
    }
}
