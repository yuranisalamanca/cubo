$("#formData").submit(function(e){
    e.preventDefault();

    if($("#valores").val() == ''){
        toastr.warning('Por favor digite el campo Operaciones');
    } else{
        var token = $('#token').val();
        var dataForm = $("#formData").serializeArray();
        var route = $("#formData").attr( "action" );
        var metodo = $("#formData").attr( "method" );
        $.ajax({
            url: route+'/',
            headers: {'X-CSRF-TOKEN': token},
            type: metodo,
            data: dataForm,
            success: function(data){
                console.log(data);
                if(data.valido != ''){
                    $("#resultado").val(data.valido);
                }else{
                    $("#resultado").val('');
                    toastr.error(data.invalido);
                }
            }
        })
    }
});
