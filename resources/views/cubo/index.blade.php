@extends('layouts.main')
@section('content')
{!!Form::open(array('url' => '/', 'method' => 'POST', 'id' => 'formData')) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="row-fluid">
    <div class="col-md-6">
        <div class="form-group">
            <label for="comment">Operaciones</label>
            <textarea class="form-control" rows="10" id="valores" name="valores"></textarea>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="comment">Resultado</label>
            <textarea class="form-control" rows="10" id="resultado"></textarea>
        </div>
    </div>

    <button type="submit" class="btn btn-lg btn-default">Ejecutar Operaciones</button>
</div>
</form>
@endsection