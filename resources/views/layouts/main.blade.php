@include('includes.tagsCSS')
</head>
<body>
<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="container">
            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">Cube - Summation</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li class="active"><a href="#">Inicio</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="inner cover">
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('includes.tagsJS')
</body>
</html>